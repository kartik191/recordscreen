import React, { Component } from 'react';
import './App.css';
import Home from './components/Home';
import Recorder from './components/Recorder';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      recorder: false
    };
  }
  changeRecorder = recorder => {
    this.setState({
      recorder
    });
  }

  render() { 
    const {recorder} = this.state;
    return (  
      <div>
        {
          !recorder ? 
            <Home changeRecorder={this.changeRecorder}/> :
            <Recorder changeRecorder={this.changeRecorder}/>
        }
      </div>
    );
  }
}

export default App;