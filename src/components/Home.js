import { Heading, Box, Button, Image, Text, Link } from "@chakra-ui/react";
import React, { Component } from "react";

export default class Home extends Component {
  render() {
    return (
      <Box px={"10vw"} textAlign="center" color="#606c76">
        <Box m="auto" mt="26vh" w={"70%"} letterSpacing={"-1px"} >
          <Image src="/images/recorderIcon.png" width={"70px"} m="auto" />
          <Heading fontSize={"46px"} fontWeight={"300"} mt={"6px"}>
            RecordScreen
            <Text as={"span"} fontSize={38}>
              .io
            </Text>
          </Heading>
          <Text fontSize={22}  mt={4} lineHeight={1.4} >
            Record your screen right from the browser.
            <br />
            No installation required.
          </Text>
          <Button
            bg={"#9b4dca"}
            mt={5}
            color="white"
            _hover={{ bg: "gray" }}
            px={8}
            fontSize={12}
            fontWeight={"bold"}
            borderRadius={3}
            onClick={() => {
              this.props.changeRecorder(true);
            }}
          >
            RECORD!
          </Button>
        </Box>
        <Box m="auto" mt={20} w="70%">
          <Text fontStyle={"italic"} fontSize={14}>
            Videos are processed in the browser and are never sent to our
            servers.
          </Text>
          <Text mt={5} fontSize={13}>
            By{" "}
            <Link color={"#9b4dca"} href="https://codeinterview.io/" _hover={{textDecoration:"none",color:"#606c76"}}>
              CodeInterview.io
            </Link>{" "}
            team. Copyright © 2022.
          </Text>
        </Box>
      </Box>
    );
  }
}
