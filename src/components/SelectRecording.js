import React, { Component } from 'react';
import { Button, Text, Box } from '@chakra-ui/react';

class SelectRecording extends Component {
    constructor(props) {
        super(props);
    }
    render() { 
        const {screenAndCamera, changeRecordingType, changeRecorder, changeStatus} = this.props;
        return (
            <div className='selection-page'>
               <Box className='selection-mode' border={"1px solid rgba(201,186,210,.25)"} boxShadow={" 17px 17px 47px -13px rgb(0 0 0 / 12%)"}>
                   {/* <h3>
                       {
                           screenAndCamera ? 'Record Screen + Camera' : 'Record Screen'
                       }
                   </h3> */}
                   <Text fontSize={22} mt={0} color="gray" lineHeight={1.4} className="selection-header">
                        {
                           screenAndCamera ? 'Record Screen + Camera' : 'Record Screen'
                        }
                    </Text>
                   <div className='logos-container'>
                       <figure className={screenAndCamera ? 'icon-container' : 'icon-container dim-icon'}>
                            <img src='https://recordscreen.io/images/icon-screencam.png' style={{width: '60px'}} onClick={() => changeRecordingType(true)}/>
                            <figcaption> Screen + Cam </figcaption>
                       </figure>
                       <figure className={!screenAndCamera ? 'icon-container' : 'icon-container dim-icon'}>
                            <img src='https://recordscreen.io/images/icon-screenonly.png' style={{width: '60px'}} onClick={() => changeRecordingType(false)}/>
                            <figcaption> Screen Only </figcaption>
                       </figure>
                   </div>
                   <div>
                        <Button
                            bg={"none"}
                            mt={5}
                            color="#9b4dca"
                            _hover={{ color: "gray" }}
                            px={8}
                            fontSize={12}
                            fontWeight={"bold"}
                            borderRadius={3}
                            onClick={() => changeRecorder(false)}
                         >
                           CANCEL
                        </Button>
                       {/* <button type='button' onClick={() => changeRecorder(false)}> CANCEL </button> */}
                       {/* <button onClick={() => changeStatus()}> START RECORDING </button> */}
                       <Button
                            bg={"#9b4dca"}
                            mt={5}
                            color="white"
                            _hover={{ bg: "gray" }}
                            px={8}
                            fontSize={12}
                            fontWeight={"bold"}
                            borderRadius={3}
                            onClick={() => changeStatus()}
                        >
                            START RECORDING
                        </Button>
                   </div>
               </Box>
               <div className='camera' style={screenAndCamera ? {display: 'block'} : {display: 'none'}}>
                    <span> Camera <br/> Placement </span>
               </div>
            </div>
          );
    }
}
 
export default SelectRecording;