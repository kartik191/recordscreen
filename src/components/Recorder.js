import React, { Component } from "react";
import SelectRecording from "./SelectRecording";
import Recording from "./Recording";
import ShowRecording from "./ShowRecording";

class Recorder extends Component {
  constructor(props) {
    super(props);
    this.state = {
    //   status: "selecting",
      screenAndCamera: true,
      streamScreen: null,
      StreamCamera: null,
      isSelected: false,
      isPlaying: false,
      isSaved: false,
      downloadVideoURL: null,
    };
  }
  mediaRecorder;
  changeRecordingType = (screenAndCamera) => {
    this.setState({
      screenAndCamera,
    });
  };
  changeStatus = () => {
    this.setState({
      isSelected: true,
    });
  };
  start = async () => {
    const displayMediaOptions = {
      video: true,
      // audio: true,
      // video: {
      //     cursor: "always"
      // },
      // audio: {
      //     echoCancellation: true,
      //     noiseSuppression: true,
      //     sampleRate: 44100
      // }
    };

    // if(screenAndCamera){
    //     let streamCam = await navigator.mediaDevices.getUserMedia({video: true});
    //     cameraEle.srcObject = streamCam;
    // }

    let videostream = await navigator.mediaDevices.getDisplayMedia(
      displayMediaOptions
    );
    let audioStream = await navigator.mediaDevices.getUserMedia({
      audio: true,
    });
    const stream = new MediaStream([
      ...videostream.getTracks(),
      ...audioStream.getTracks(),
    ]);

    this.mediaRecorder = this.createRecorder(stream, "video/webm");

    this.setState({
      streamScreen: stream,
      isPlaying: true,
    });
  };
  stop = () => {
    const tracks = this.state.streamScreen.getTracks();
    tracks.forEach((track) => track.stop());

    this.mediaRecorder.stop();

    // cameraEle.srcObject.getTracks().forEach(track => track.stop());
    // cameraEle.srcObject = null;
  };

  createRecorder = (stream, mimeType) => {
    let recordedChunks = [];

    const mediaRecorder = new MediaRecorder(stream);

    mediaRecorder.ondataavailable = function (e) {
      if (e.data.size > 0) {
        recordedChunks.push(e.data);
      }
    };
    mediaRecorder.onstop = () => {
      this.saveFile(recordedChunks);
      recordedChunks = [];
    };
    mediaRecorder.start(200); // For every 200ms the stream data will be stored in a separate chunk.
    return mediaRecorder;
  };

  saveFile = (recordedChunks) => {
    const blob = new Blob(recordedChunks, {
      type: "video/webm",
    });
    // let filename = 'record-video';
    this.setState({
      downloadVideoURL: URL.createObjectURL(blob),
      isPlaying: false,
      isSaved: true,
    });

    // downloadLink.href = URL.createObjectURL(blob);
    // downloadLink.download = `${filename}.webm`;

    // videoEle.src = URL.createObjectURL(blob);   // ????????
    // URL.revokeObjectURL(blob); // clear from memory
  };

  render() {
    const { status } = this.state;
    if (this.state.isSaved) {
      return <ShowRecording downloadVideoURL={this.state.downloadVideoURL} changeRecorder={this.props.changeRecorder}/>;
    }
    return (
      <div>
        {!this.state.isSelected ? (
          <SelectRecording
            screenAndCamera={this.state.screenAndCamera}
            changeRecordingType={this.changeRecordingType}
            changeRecorder={this.props.changeRecorder}
            changeStatus={this.changeStatus}
          />
        ) : (
          <Recording
            start={this.start}
            stop={this.stop}
            isPlaying={this.state.isPlaying}
            isSaved={this.state.isSaved}
            stream={this.state.streamScreen}
            downloadVideoURL={this.state.downloadVideoURL}
          />
        )}
      </div>
    );
  }
}

export default Recorder;
