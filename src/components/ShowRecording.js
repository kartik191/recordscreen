import React, { Component } from 'react';
import { Box, Text, Button, Link } from "@chakra-ui/react";

class ShowRecording extends Component {
    constructor(props) {
        super(props);
    }
    render() {  
        let filename = 'recorded-video';
        return ( 
            <div>
                <Box
                color="#606c76"
                mt="26vh"
                w={{ base: "47%", sm: "47%", md: "38%" }}
                mx="auto"
                border={"1px solid rgba(201,186,210,.25)"}
                boxShadow={" 17px 17px 47px -13px rgb(0 0 0 / 12%)"}
                rounded="5px"
                p={5}
                textAlign="center"
                        >
                    <Text fontSize="22px" mb={4}>
                    Download Video
                    </Text>

                    <Box
                    as="video"
                    src={this.props.downloadVideoURL} 
                    controls
                    type="video/webm"                    
                    objectFit="contain"                    
                    mb={10}
                    />
                    <Link href={this.props.downloadVideoURL} download={`${filename}.webm`}>
                        <Button
                        bg={"#9b4dca"}
                        color={"white"}
                        _hover={{ bg: "gray" }}
                        fontSize="xs"
                        px={12}
                        alignSelf={"flex-end"}
                        >
                        {" "}
                        Download Video
                        </Button>
                    </Link>
                    <Button
                    bg={"white"}
                    color={"#9b4dca"}
                    border={"1px solid #9b4dca"}
                    _hover={{ color: "gray", border: "1px solid gray"}}
                    fontSize="xs"
                    px={12}
                    alignSelf={"flex-end"}
                    onClick={() => this.props.changeRecorder(false)}
                    >
                    {" "}
                    CLOSE
                    </Button>
                </Box>
            </div>
         );
    }
}

export default ShowRecording;