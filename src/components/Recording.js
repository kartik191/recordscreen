import React, { Component } from "react";
import { Box, Text, Button } from "@chakra-ui/react";

class Recording extends Component {
  constructor(props) {
    super(props);
    this.videoRef = React.createRef();
  }
  componentDidMount() {
    this.props.start();
  }
  componentDidUpdate() {
    this.updateVideoStream();
  }

  updateVideoStream() {
    if (this.videoRef.current.srcObject !== this.props.stream) {
      this.videoRef.current.srcObject = this.props.stream;
    }
  }
  render() {
    if (!this.props.isPlaying) {
      return (
        <Box
          mt={"26vh"}
          ml={{ base: "28%", sm: "28%", md: "31%" }}
          border={"1px solid rgba(201,186,210,.25)"}
          p={"20px"}
          boxShadow={" 17px 17px 47px -13px rgb(0 0 0 / 12%)"}
          rounded="5px"
          w={{ base: "44%", sm: "44%", md: "37%" }}
          color="#606c76"
        >
          <Text
            textAlign="center"
            fontSize={"22px"}
            lineHeight={1.4}
            mb={5}
            letterSpacing={"-1px"}
          >
            Getting permissions for mic and screen...
          </Text>
        </Box>
      );
    }
    
    return (
      <Box
        color="#606c76"
        mt="26vh"
        w={{ base: "47%", sm: "47%", md: "38%" }}
        mx="auto"
        border={"1px solid rgba(201,186,210,.25)"}
        boxShadow={" 17px 17px 47px -13px rgb(0 0 0 / 12%)"}
        rounded="5px"
        p={5}
        textAlign="center"
      >
        <Text fontSize="22px" mb={4}>
          Recording in progress
        </Text>

        <Box
          as="video"
          ref={this.videoRef}
          autoPlay
          type="video/webm"
         
          objectFit="contain"
         
          mb={10}
        />
        {/* <Text>
          Could not get camera stream. Did you give camera permissions?
        </Text>
        <Text>
          {" "}
          Could not get microphone stream. Did you give microphone permissions?
        </Text> */}
        <Button
          bg={"#9b4dca"}
          color={"white"}
          _hover={{ bg: "gray" }}
          fontSize="xs"
          px={12}
          alignSelf={"flex-end"}
          onClick={this.props.stop}
        >
          {" "}
          Stop Recording
        </Button>
      </Box>
    );
  }
}

export default Recording;
